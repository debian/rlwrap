rlwrap (0.46.1-1) unstable; urgency=medium

  * New upstream release (Closes: #986294, #977052, #959009)
  * Additional packaging changes:
    * d/control:
      - Updated Maintainer field due to ITS process (Closes: #1010584)
      - Updated Standards-Version
      - Updated debhelper-compat version
    * d/patches/man-typos.patch: Removed patch (no longer applies)
    * d/docs: README is now README.md in source.
    * Replace CI config with central one

 -- Thomas Ward <teward@ubuntu.com>  Tue, 08 Nov 2022 18:16:15 -0500

rlwrap (0.43-2) UNRELEASED; urgency=medium

  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Fix misspelling of Close => Closes.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 25 Apr 2020 22:55:54 +0000

rlwrap (0.43-1) unstable; urgency=medium

  * New upstream version 0.43 (Closes: #869564)
    - Fix segmentation fault with readline keyboard macros (Closes: #799943)
    - Fix internal memory error on Tab key with --filter (LP: #1731508)
    - Make build system amenable to cross-compilation (Closes: #874217)
  * d/p/update-project-urls.patch: Drop patch, applied upstream
  * d/p/man-typos.patch: Replace patch, fix man page typos in new release
  * Bump debhelper compatibility level to 11
  * d/control, d/rules: Drop explicit dependency on dh-autoreconf, now used
    automatically
  * d/clean: Clean files remaining after a successful build
  * d/copyright: Bump copyright for release and for debian/*
  * d/rules:
    - Include dh_python3 in the build sequence
    - Rewrite perl shebang to comply with Policy 4.1.2
    - Don't backup and restore build files no longer distributed
  * d/control:
    - Build-Depend on dh-python and python3:any and Depend on ${python3:depends}
    - Update Vcs-* fields for migration to salsa.d.o
    - Bump Standards-Version to 4.1.3

 -- Mike Miller <mtmiller@debian.org>  Sun, 25 Feb 2018 12:46:03 -0800

rlwrap (0.42-3) unstable; urgency=medium

  * d/p/man-typos.patch: Refresh header, applied upstream
  * d/p/update-project-urls.patch: Fix references to old project URL
  * d/watch: Use new upstream release URL.
  * d/control: Reorder fields according to "cme fix dpkg-control"
  * d/copyright:
    - Update Format using "cme fix dpkg-copyright"
    - Update upstream Source URL
    - Bump copyright to 2016 for debian/*

 -- Mike Miller <mtmiller@debian.org>  Sat, 24 Dec 2016 14:35:33 -0800

rlwrap (0.42-2) unstable; urgency=medium

  * Upload to unstable.
  * Run wrap-and-sort on debian/docs.
  * Use https URI for Vcs-* control fields.
  * d/p/man-typos.patch: Fix typos in rlwrap man page.
  * Update the upstream Homepage URL.
  * Bump Standards-Version to 3.9.8. No changes needed.

 -- Mike Miller <mtmiller@debian.org>  Fri, 25 Nov 2016 17:21:37 -0800

rlwrap (0.42-1) experimental; urgency=medium

  * New upstream release.
    - Fix spurious error due to stale errno. (Closes: #779692)
  * tab-completion-prefix.patch: Drop, applied upstream.
  * Bump Standards-Version to 3.9.6. No changes needed.

 -- Mike Miller <mtmiller@debian.org>  Mon, 06 Apr 2015 00:19:44 -0400

rlwrap (0.41-1) unstable; urgency=medium

  * New upstream version.
  * pselect-timeout-optimized-away.patch: Drop, applied upstream.
  * tab-completion-prefix.patch: Allow input text to be its own valid
    completion. (Closes: #648264)
  * Use dh-autoreconf to regenerate the build system, drop autotools-dev.
  * Add Depends on ${perl:Depends} substvar.
  * Run wrap-and-sort on debian/control.

 -- Mike Miller <mtmiller@debian.org>  Fri, 12 Sep 2014 01:13:54 -0400

rlwrap (0.37-5) unstable; urgency=medium

  * debian/patches/pselect-timeout-optimized-away.patch: Ensure rlwrap exits
    when its child process exits. (Closes: #730718)
  * Use my @debian.org address.
  * Bump Standards-Version to 3.9.5. No changes needed.

 -- Mike Miller <mtmiller@debian.org>  Sun, 23 Feb 2014 11:24:00 -0500

rlwrap (0.37-4) unstable; urgency=low

  * debian/rules:
    - Build with autotools-dev using dh addon to support new
      architectures. (Closes: #727955)
    - Update timestamp on generated source file to avoid executing rule
      to regenerate. (Closes: #717367)
    - Build with --as-needed to reduce library dependencies.
  * Bump debhelper compatibility level to 9.
  * Canonicalize Vcs-* control fields.
  * Bump Standards-Version to 3.9.4. No changes needed.

 -- Mike Miller <mtmiller@ieee.org>  Wed, 30 Oct 2013 03:26:06 -0400

rlwrap (0.37-3) unstable; urgency=low

  * New maintainer (Closes: #654934)
  * Update debian/copyright to machine-readable format 1.0
  * Bump Standards-Version to 3.9.3 (no changes)

 -- Mike Miller <mtmiller@ieee.org>  Wed, 11 Apr 2012 08:17:52 -0400

rlwrap (0.37-2) unstable; urgency=low

  * Fix watch file
  * Bump Standards-Version up to 3.9.1
  * Bump debhelper compatibility to 8

 -- Francois Marier <francois@debian.org>  Wed, 22 Dec 2010 17:11:31 +1300

rlwrap (0.37-1) unstable; urgency=medium

  * New upstream version:
    - fix for FTBFS on armel (closes: #580174)
  * Urgency set to medium since it fixes an RC-bug

 -- Francois Marier <francois@debian.org>  Thu, 06 May 2010 09:12:29 +1200

rlwrap (0.36-2) unstable; urgency=low

  * Bump Standards-Version up to 3.8.4 (no changes)

 -- Francois Marier <francois@debian.org>  Sun, 04 Apr 2010 15:37:35 +1200

rlwrap (0.36-1) unstable; urgency=low

  * New upstream version

 -- Francois Marier <francois@debian.org>  Mon, 18 Jan 2010 21:58:58 +1300

rlwrap (0.35-1) unstable; urgency=low

  * New upstream version:
    - drop manpage hyphen patch (committed upstream)

 -- Francois Marier <francois@debian.org>  Sat, 09 Jan 2010 10:15:43 +1300

rlwrap (0.34-2) unstable; urgency=low

  * Fix FTBS on avr32 as suggested on #559098 (closes: #561798)

 -- Francois Marier <francois@debian.org>  Wed, 23 Dec 2009 13:27:16 +1300

rlwrap (0.34-1) unstable; urgency=low

  * New upstream release:
    - fix bug when repeating commands (closes: #560859)
  * Switch to "3.0 (quilt)" source format
  * Add a patch to fix hyphens in one of the manpages

 -- Francois Marier <francois@debian.org>  Sun, 20 Dec 2009 12:19:49 +1300

rlwrap (0.33-1) unstable; urgency=low

  * New upstream version:
    - fix for garbage printed out to the screen (closes: #557070)

 -- Francois Marier <francois@debian.org>  Fri, 27 Nov 2009 10:50:17 +1300

rlwrap (0.32-1) unstable; urgency=low

  * New upstream release
  * Adopt package (closes: #556560)
  * Acknowledge NMU (closes: #522931)
  * Change readline dependency to libreadline-dev (closes: #553838)
  * Use a minimal debian/rules file (closes: #534826)

  * Bump Standards-Version up to 3.8.3
  * Bump debhelper compatibility to 7
  * Create debian/docs
  * Move -e in postinst and prerm so that lintian finds it
  * Add misc:Depends (lintian warning)
  * Add git collab-maint VCS fields to debian/control

 -- Francois Marier <francois@debian.org>  Wed, 18 Nov 2009 12:30:00 +1300

rlwrap (0.30-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream release (Closes: #522931)
  * Add missing debian/watch file
  * Bump Standards-Version to 3.8.1 (no changes needed)
  * Bump debhelper compatibility level to 5 (4 is now deprecated)
  * Add Homepage field to debian/control
  * debian/rules: install upstream ChangeLog
  * debian/copyright: add copyright notice
  * Register rlwrap as a provider of "readline-editor". Make the package
    both provide the corresponding virtual package and register as an
    alternative for /usr/bin/readline-editor. (Closes: #522929)

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 07 Apr 2009 14:40:22 +0200

rlwrap (0.28-2) unstable; urgency=medium

  * Changed debian/rules to use $(CURDIR) instead of $(PWD) so package
    building works in sudo based environments.  (Closes: #403952).

 -- Oyvind Gronnesby <oyving@pvv.org>  Thu, 21 Dec 2006 15:00:21 +0100

rlwrap (0.28-1) unstable; urgency=low

  * New upstream release. (Closes: #401993).
  * Updated the Standards-Version field to 3.7.2.  No other changes were
    needed for policy compliance.

 -- Oyvind Gronnesby <oyving@pvv.org>  Sat,  9 Dec 2006 20:19:59 +0100

rlwrap (0.24-1) unstable; urgency=low

  * New upstream release.

 -- Oyvind Gronnesby <oyving@pvv.org>  Sun, 30 Oct 2005 01:30:25 +0200

rlwrap (0.21-1) unstable; urgency=low

  * New upstream release.
  * Build-Depends changed from libreadline4-dev to libreadline5-dev.
  * Fixed man-page so that it has correct use of hyphens.

 -- Oyvind Gronnesby <oyving@pvv.org>  Wed, 16 Mar 2005 00:15:42 +0100

rlwrap (0.18-1) unstable; urgency=low

  * New upstream release.
    * Fixes hang bug when rlwrap exits too fast after called with
      no legal command or command exits too fast. (Closes: #229790).
  * Added compat file for debhelper, removed conffiles files.
  * Made the clean script delete doc/rlwrap.1.

 -- Oyvind Gronnesby <oyving@pvv.org>  Sun, 29 Feb 2004 00:21:27 +0100

rlwrap (0.17-3) unstable; urgency=low

  * Another severe error in rules file fixed. (Closes: #229751)
  * Removed the need for DH_COMPAT, which resulted in the adding of
    debian/conffiles and removal of several useless variables.

 -- Oyvind Gronnesby <oyving@pvv.org>  Mon, 26 Jan 2004 14:30:00 +0100

rlwrap (0.17-2) unstable; urgency=low

  * More polishing of rules file.
  * Fixed building error and made rules comply with Policy. (Closes: #229415)

 -- Oyvind Gronnesby <oyving@pvv.org>  Sat, 24 Jan 2004 20:00:00 +0100

rlwrap (0.17-1) unstable; urgency=low

  * New upstream release.
  * Fixes abaondoned files in /etc/rlwrap, we now have completion lists
    in /etc/rlwrap again to comply with Debian policy. (Closes: #217354)
  * New maintainer. (Closes: #215555)
  * Polished the rules file.

 -- Oyvind Gronnesby <oyving@pvv.org>  Sat, 24 Jan 2004 16:25:47 +0100

rlwrap (0.16-1) unstable; urgency=low

  * new upstream release (closes: #208828)
  * updated to follow the latest version of the Debian Policy

 -- Marco Kuhlmann <mk@debian.org>  Thu,  9 Oct 2003 16:07:33 +0200

rlwrap (0.15-1) unstable; urgency=low

  * initial release

 -- Marco Kuhlmann <mk@debian.org>  Fri, 21 Mar 2003 16:21:23 +0100
